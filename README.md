# Text Cannot Give the Whole Picture

There are many different ways text can present itself. Text can be read in a more traditional way through typed words
in books and documents, or in ways that humans cannot fully process such as algorithims and computer code. However, no
matter how well someone can describe something, either through imagery or statistics, there is a likelyhood that the 
human mind reading it will not visuaize the exact same image as the author.

In Steven Shaviro's *Connected* he writes about how he cannot adequately describe his dreams through words, so that the
person listening would be able to see eaxactly what he did.
> "I often try to recount my dreams in words, but I cannot help feeling that such words are woefully inadequate. If I
if I cannot remember my own dreams, if I cannot narrate them to myself any more than I can to someone else, if I cannot
translate them into the generalities of language, this only confirms how unique and personal they are" (Shaviro 25).

What Chaviro writes about describing his dreams, is an acurate depiction of what it is like to describe many personal
thoughts and experiences through words. I could write out a specific memory, and have ten people read it. No matter how many
details I give, or how many adjectives I use, every person would imagine the situation at leasat slightly differently. It
is the same when reading a book. Whether it's a mystery novel, fantasy, a memoir- no one reading the same book imagines
the characters and events exactly the same way. It works the same way with theater, and many other types of art. A play could 
be performed a thousand times, and each time it would be different based on the director's vision and interpretation of the text-even
though the text itself stays the same.

The same issue arises when giving statistics and/or facts. When trying to figure out where to get somewhere new, I could 
read and follow directions. However I wouldn't know exactly where I am, in relation to other things, without a visual
(like a map). Or, I could read an entire book that has facts and statistics of a certain city, but I would not be able to
imagine exactly how the city looks without a picture-or actually visiting.

This is why pictures are so important if you need people to have the same visuals in their minds. For example, when
people lose a pet, they post flyers with a picture of their pet. It would be diffiuclt for someone to find a pet if
the flyer just said, "black dog". Everyone would havea  different image of a black dog in their head, and not know 
exactly what to be looking for.

![alt text](https://upload.wikimedia.org/wikipedia/commons/4/4a/Black_Labrador_Retriever_portrait.jpg width="200" height="50%"})
![alt text](http://cdn.earthporm.com/wp-content/uploads/2014/08/desktop-1406691638.png.jpg height="150")

However, leaving things to peoples' imaginations can often be a good thing- and is what causes creativity, new ideas, and
different perspectives. If every person had the same idea of how things were supposed to look, there would be no need
for art, theater and movie directors, or writers, because no one would want, or need, anything described or shown to
them. They would already know, and know that everyone is thinking the exact same thing. 

[This is a video on imagination, and ideas.](https://www.youtube.com/watch?v=90cfz2JHx1U)

![alt text](http://www.planwallpaper.com/static/images/best-scenery-wallpaper-21.jpg width="200")
